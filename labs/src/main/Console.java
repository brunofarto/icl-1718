package main;

import java.io.ByteArrayInputStream;

import ast.ASTNode;
import parser.ParseException;
import parser.Parser;
//import parser.SimpleParser;
import parser.TokenMgrError;
import types.IType;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.Environment;
import util.UndeclaredIdentifierException;
import values.DynamicTypingException;
import values.IValue;
import values.IntValue;

public class Console {

	@SuppressWarnings("static-access")
	public static void main(String args[]) {
		Parser parser = new Parser(System.in);

		while (true) {
			try {
				ASTNode n = parser.Start();
				Environment<IValue> env = new Environment<>();
				Environment<IType> tenv = new Environment<>();
				n.typecheck(tenv);
				System.out.println("Type= " + n.getType().toString() );
				System.out.println("OK! = " + n.eval(env) );
			} catch (TokenMgrError e) {
				System.out.println("Lexical Error!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (ParseException e) {
				System.out.println("Syntax Error!");
				e.printStackTrace();
				parser.ReInit(System.in);
			} catch (TypingException e) {
				System.out.println("Typing Error!");
				e.printStackTrace();
			} catch (DynamicTypingException e) {
				System.out.println("Dynamic Typing Error!");
				e.printStackTrace();
			} catch (UndeclaredIdentifierException e) {
				System.out.println("Undeclared Identifier Error!");
				e.printStackTrace();
			} catch (DuplicateIdentifierException e) {
				System.out.println("Duplicate Identifier Error!");
				e.printStackTrace();
			}
		}
	}

	public static boolean accept(String s) throws ParseException {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			parser.Start();
			return true;
		} catch (TokenMgrError e) {
			return false;
		} catch (ParseException e) {
			return false;
		}
	}

	public static boolean acceptCompare(String s, int value) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			ASTNode n = parser.Start();
			Environment<IValue> env = new Environment<>();
//			return n.eval() == value;
			return n.eval(env) == new IntValue(value);
		} catch (TokenMgrError e) {
			return false;
		} catch (ParseException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean acceptCompare(String s, IValue value) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			ASTNode n = parser.Start();
			Environment<IValue> env = new Environment<>();
			return n.eval(env).equals(value);
		} catch (TokenMgrError e) {
			return false;
		} catch (ParseException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
	}

	public static boolean acceptCompareTypes(String s, IType type) {
		Parser parser = new Parser(new ByteArrayInputStream(s.getBytes()));
		try {
			ASTNode n = parser.Start();
			Environment<IType> tenv = new Environment<>();
			return n.typecheck(tenv).equals(type);
		} catch (TokenMgrError e) {
			return false;
		} catch (ParseException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
	}


}
