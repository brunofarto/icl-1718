package tests;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

import main.Console;
import parser.ParseException;
import values.BoolValue;
import values.IValue;
import values.IntValue;

public class InterpreterTests {

	private void testCase(String expression, IValue value) throws ParseException {
		assertTrue(Console.acceptCompare(expression,value));		
	}
	
	private void testNegativeCase(String expression, IValue value) throws ParseException {
		assertFalse(Console.acceptCompare(expression, value));
	}
	
	// private void testCaseBool(String expression, boolean value) throws ParseException {
	// 	assertTrue(Console.acceptCompareBool(expression, new BoolValue(value)));		
	// }
	
	// private void testNegativeCaseBool(String expression, boolean value) throws ParseException {
	// 	assertFalse(Console.acceptCompareBool(expression, new BoolValue(value)));
	// }
	
	@Test
	public void test01() throws Exception {
		testCase("1;;",new IntValue(1));
		testCase("1+2;;",new IntValue(3));
		testCase("1-2-3;;",new IntValue(-4));
	}
	
	@Test
	public void testsLabClass02() throws Exception {
		testCase("4*2;;",new IntValue(8));
		testCase("4/2/2;;",new IntValue(1));
		testCase("-1;;", new IntValue(-1));
		testCase("-1*3;;",new IntValue(-3));
		testCase("!false;;",new BoolValue(true));
		testCase("!true;;",new BoolValue(false));
	}
	
	@Test
	public void testsLabClass05() throws Exception {
		testCase("decl x = 1 in x+1 end;;",new IntValue(2));
		testCase("decl x = 1 in decl x = x in x end + 1 end;;",new IntValue(2));
		testCase("decl x = 1 in decl y = 2 in x+y end end;;",new IntValue(3));
		testCase("decl x = decl y = 2 in 2*y end in x*3 end;;",new IntValue(12));
		testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y end;;", new IntValue(6));
		testNegativeCase("x+1;;", new IntValue(0));
		testNegativeCase("decl x = 1 in x+1 end + x;;", new IntValue(0));
		//testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y+x end\n", new IntValue(5));		
	}
	
	@Test
	public void testsLabClass07() throws Exception {
		testCase("*var(0);;",new IntValue(0));
		testCase("*var(99);;",new IntValue(99));
		testCase("var(0):=1;;",new IntValue(1));
		testCase("decl x = var(0) in x := 1; *x end;;",new IntValue(1));
		testCase("decl x = var(0) in decl y = x in x := 1; *y end end;;",new IntValue(1));
		testCase("decl x = var(0) in decl y = var(0) in x := 1; *y end end;;",new IntValue(0));
		testCase("decl x = var(0) in decl y = var(0) in while *x < 10 do y := *y + 2; x := *x + 1 end; *y end end;;",new IntValue(20));
		testCase("if true then 1 else 2 end;;", new IntValue(1));
		testCase("if false then 1 else 2 end;;", new IntValue(2));
		testNegativeCase("if true then true else 1 end;; ", new IntValue(0)); // type test
		testCase("decl x = var(3) in "
				+ 	"decl y = var(1) in "
				+ 		"while *x > 0 do "
				+ 			"y := *y * *x; "
				+ 			"x := *x - 1 "
				+ 		"end;"
				+ 		" *y "
				+ 	"end "
				+ "end;;",new IntValue(6));
		}
/*
decl x = var(3) in
	decl y = var(1) in
		while *x > 0 do
			y := *y * *x;
			x := *x - 1
		end;
		*y
	end
end;;
*/
}
