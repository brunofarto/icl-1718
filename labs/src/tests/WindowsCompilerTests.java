package tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import org.junit.Test;

import main.Compiler;
import main.Console;
import parser.ParseException;
import types.TypingException;
import util.DuplicateIdentifierException;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class WindowsCompilerTests {
	/*
	 * Parametrise it with the directory in which your jasmin.jar resides,
	 * relative to your project's top directory
	 * 
	 * e.g. My eclipse project is a directory called ICL with
	 * subdirectories notes, slides and labs. My labs folder
	 * has a subdirectory src, and jasmin.jar is inside src.
	 */
	private static final File dir = new File("src");
	
	private void testCase(String expression, String result) 
			throws IOException, InterruptedException, ParseException, FileNotFoundException, UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
		Process p;
		
		p = Runtime.getRuntime().exec(new String[]{"cmd", "/c", "del *.j *.class"}, null, dir);
	    p.waitFor();
	    
	    System.out.println("Compiling to Jasmin source code");

	    Compiler.compile(expression);
		
	    System.out.println("Compiling to Jasmin bytecode");
	    
		p = Runtime.getRuntime().exec(new String[]{"cmd", "/c", "java -jar jasmin.jar *.j"}, null, dir);
	    p.waitFor();	    
	    assertTrue("Compiled to Jasmin bytecode", p.exitValue() == 0);

	    BufferedReader reader = 
		         new BufferedReader(new InputStreamReader(p.getInputStream()));

	    StringBuffer output = new StringBuffer();
        String line = "";			
        while ((line = reader.readLine())!= null) {
        		output.append(line + "\n");
        }
	    System.out.println(output.toString());

		p = Runtime.getRuntime().exec(new String[] {"cmd","/c", "java Demo"}, null, dir);
	    p.waitFor();

	    reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

	    output = new StringBuffer();
        line = "";			
        while ((line = reader.readLine())!= null) {
        		output.append(line + "\n");
        }
	    System.out.println("Output: #"+output.toString()+"#");
	    
	    assertTrue(result.equals(output.toString()));
	}
	
	private void testCase(String expression, int value) throws FileNotFoundException, IOException, InterruptedException, ParseException, UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
		testCase(expression, value+"\n");		
	}
	
	private void testNegativeCase(String expression, int value) throws ParseException {
		assertFalse(Console.acceptCompare(expression, value));
	}

	@Test
	public void BasicTest() throws IOException, InterruptedException, ParseException, UndeclaredIdentifierException, TypingException, DuplicateIdentifierException{
		testCase("1;;", 1);
	}

	@Test
	public void testsLabClass02() throws Exception {
		testCase("1+2;;",3);
		testCase("1-2-3;;",-4);
		testCase("4*2;;",8);
		testCase("4/2/2;;",1);
	}
	
	@Test
	public void testsLabClass03() throws Exception {
		testCase("1<2;;",1);
		testCase("1<=0;;",0);
		testCase("4==2;;",0);
		testCase("4>=2;;",1);
	}
	
	@Test
	public void testsDecl1() throws Exception {
		testCase("decl x = 1 y = 1 in x+y end;;",2);
		testCase("decl x = 1 in decl y = 2 in x+y end end;;",3);
		testCase("decl x = 1 in decl x = x in x end + 1 end;;",2);
		testCase("decl x = decl y = 2 in 2*y end in x*3 end;;",12);
		testCase("decl x = 1 in x+2 end * decl y = 1 in 2*y end;;",6);
		testNegativeCase("decl x = 1 in x+2 end * decl y = 1 in 2*y+x end;;",5);
		testNegativeCase("decl x = 1 in x+1 end + x;;", 0);
	}
	
	@Test
	public void testsDecl2() throws Exception {
		testCase("decl y = decl x = 3 in x * 4 end in y + 2 end;;",14);
		testCase("decl x = 6 in decl x = 3 y = decl z = 7 + x in z * x end in y + x end end;;",81);
		testCase("decl x = 6 in decl x = decl z = 2 in z * z * z end y = decl z = 7 + x in z * x end in y + x end end;;",86);		
	}
	
	@Test
	public void testsDecl0() throws Exception {
		testCase("decl x = true in x && false end;;",0);
		testCase("decl x = decl z = 2 in !(2 <= z) end y = !true in !(x || y) end;;",1);		
		testCase("decl x = !true y = false in !(x || y) end;;",1);
		testCase("decl x = decl x = 1 in x >= 2 end y = decl x = 34 in x*17 end in decl z = 3 in !x && (y == z) end end;;",0);
		testCase("decl x = 1 in x < 2 end && decl y = 1 in 2 <= y end;;",0);
		testCase("decl x = decl y = 3 in y != 2 end in x && true end;;",1);
		testCase("decl x = false in decl y = true in x || y end end;;",1);
		testCase("decl x = !true y = decl z = 2 in !(2 <= z) end in decl w = 2 t = !y in (w != 1) && !(x || y) || t end end;;",1);
		
	}
	
    @Test
    public void testVariables() throws Exception {
    	testCase("decl x = var(1) in *x end;;", 1);
    	testCase("***var(var(var(1)))+*var(1);;", 2);
    	testCase("*var(true);;", 1);
    }
    
	@Test
	public void testVariables1() throws Exception {
		testCase("decl x = var(var(1)) in *x := **x + 1; **x end;;", 2);
		testCase("decl x = var(0) in x := 2; *x end;;", 2);
		testCase("decl x = var(0) in decl y = x := *x + 1 in 2 * *x end end;;", 2);
		testCase("decl x = var(1) y = var(2) in *y * *x end;;", 2);
		testCase("decl x = var(1) in decl y = var(x) in x := **y+2 end end;;", 3);
		testCase("decl x = 1 in decl y = 1 + x in y+x end end;;", 3);
	}
 
}
