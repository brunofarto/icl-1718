package compiler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import ast.ASTDecl;
import compiler.CodeBlock.IFrame;
import compiler.CodeBlock.StackFrame;
import compiler.CodeBlock.TypeFrame;
import types.BoolType;
import types.IType;
import types.IntType;
import types.RefType;
import types.TypingException;
import util.CompilerEnv;
import values.IValue;
import ast.ASTDecl.Declaration;
import ast.ASTNode;
import ast.ASTVar;

public class CodeBlock {
	public interface IFrame {
		void dump();
	}
	
	public static class StackFrame implements IFrame{
		public int number;
		ArrayList<Declaration> decls;
		public StackFrame parentFrame;
		private static int fnumber = 0;
		
		StackFrame (ArrayList<Declaration> decls, StackFrame parentFrame) {
			this.number = fnumber++;
			this.decls = decls;
			this.parentFrame = parentFrame;
		}

		public String getName() {
			return "frame_"+number;
		}
		
		
		public void dump() {
			// open file�  
			String frameName = "frame_"+number;
	    	File file = new File(dir+frameName+".j");
//				    	files.push(file);

			BufferedWriter bw;
			
			// write header
			try {
				bw = new BufferedWriter(new FileWriter(file));
	   	
				bw.write(".source "+frameName+".j");
				bw.newLine();
				bw.write(".class "+frameName);
				bw.newLine();
				bw.write(".super java/lang/Object");
				bw.newLine();
				bw.write(".implements frame");
				bw.newLine();
				bw.newLine();
				if( this.parentFrame.number != 0 ){
					bw.write(".field public SL Lframe_"+this.parentFrame.number+";");
					bw.newLine();
					bw.newLine();
				}		
				// write ndecls field declarations
				int i = 0;
				for(Declaration id:decls){
					//String type = classToJasmin(id.getExpr());
					String type = CodeBlock.getClass(id.getType());
					bw.write(".field public loc_"+i+" "+(type.equals("int")?"I":"L"+type+";"));

					//bw.write(".field public loc_"+i+" "+(type.equals("I")?type:type+";"));
					//bw.write(".field public loc_"+i+" I");
					bw.newLine();
					i++;
				}

				// write footer
				bw.newLine();
				bw.write(".method public <init>()V");
				bw.newLine();
				bw.write("aload_0");
				bw.newLine();
				bw.write("invokespecial java/lang/Object/<init>()V");
				bw.newLine();
				bw.write("return");
				bw.newLine();
				bw.write(".end method");
				bw.flush();
	        	bw.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (TypingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			fnumber = 0;
		}

	}
	
	public static class TypeFrame implements IFrame {
		private String type;
		
		
		public TypeFrame(String type){
			this.type = type;
		}
		
		public String getType(){
			return type;
		}

		@Override
		public void dump() {
			// TODO Auto-generated method stub			// open file�  
	    	File file = new File(dir+type+".j");
			BufferedWriter bw;
			
			// write header
			if (!file.exists()) {
				try {
					bw = new BufferedWriter(new FileWriter(file));
		   	
					bw.write(".source "+type+".j");
					bw.newLine();
					bw.write(".class "+type);
					bw.newLine();
					bw.write(".super java/lang/Object");
					bw.newLine();
					bw.newLine();
					
					if(type.equals("ref_int"))
						bw.write(".field public value I");
					else
						bw.write(".field public value L"+type.substring(4)+";");
	
					// write footer
					bw.newLine();
					bw.newLine();
					bw.write(".method public <init>()V");
					bw.newLine();
					bw.write("aload_0");
					bw.newLine();
					bw.write("invokespecial java/lang/Object/<init>()V");
					bw.newLine();
					bw.write("return");
					bw.newLine();
					bw.write(".end method");
					bw.flush();
		        	bw.close();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		}
		
	}
	
	ArrayList<String> code;
	StackFrame currentFrame;
	ArrayList<IFrame> frames;
	HashMap<String,String> refClass;
	
	public static final LabelFactory labelFactory = new LabelFactory();
	private static final String dir = "./" + "src/";

	public CodeBlock() {
		code = new ArrayList<String>(100);
		frames = new ArrayList<IFrame>();
		currentFrame = new StackFrame(null,null);
		refClass = new HashMap<String,String>();
		
        File file = new File(dir+"frame.j");
		if (!file.exists()) {
			try {
				file.createNewFile();
				FileWriter fw = new FileWriter(file.getAbsoluteFile());
				BufferedWriter bw = new BufferedWriter(fw);
				bw.write(".source frame.j");
				bw.newLine();
				bw.write(".interface public frame");
				bw.newLine();
				bw.write(".super java/lang/Object");
				bw.newLine();	
				bw.flush();
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}	
		}
	}

	public void emit_push(int n) {
		code.add("       sipush "+n);
	}
	
	public void emit_dup(){
		code.add("       dup					;");
	}

	public void emit_add() {
		code.add("       iadd");
	}

	public void emit_mul() {
		code.add("       imul");
	}

	public void emit_div() {
		code.add("       idiv");
	}

	public void emit_sub() {
		code.add("       isub");
	}
	
	public void emit_and() {
		code.add("       iand");
	}

	public void emit_or() {
		code.add("       ior");
	}
	
	public void emit_not() {
		emit_push(1);
		code.add("       ixor");
	}

	void dumpHeader(PrintStream out) {
		out.println(".class public Demo");
		out.println(".super java/lang/Object");
		out.println("");
		out.println(";");
		out.println("; standard initializer");
		out.println(".method public <init>()V");
		out.println("   aload_0");
		out.println("   invokenonvirtual java/lang/Object/<init>()V");
		out.println("   return");
		out.println(".end method");
		out.println("");
		out.println(".method public static main([Ljava/lang/String;)V");
		out.println("       ; set limits used by this method");
		out.println("       .limit locals 10");
		out.println("       .limit stack 256");
		out.println("");
		out.println("       ; setup local variables:");
		out.println("");
		out.println("       ;    1 - the PrintStream object held in java.lang.out");
		out.println("       getstatic java/lang/System/out Ljava/io/PrintStream;");
		out.println("");
		out.println("       ; place your bytecodes here");
		out.println("       ; START");
		out.println("");
	}

	void dumpFooter(PrintStream out) {
		//out.println("       		aconst_null");
		//out.println("       		astore 0");
		out.println("       ; END");
		out.println("");
		out.println("");		
		out.println("       ; convert to String;");
		out.println("       invokestatic java/lang/String/valueOf(I)Ljava/lang/String;");
		out.println("       ; call println ");
		out.println("       invokevirtual java/io/PrintStream/println(Ljava/lang/String;)V");
		out.println("");		
		out.println("       return");
		out.println("");		
		out.println(".end method");
	}

	void dumpCode(PrintStream out) {
		for( String s : code )
			out.println("       "+s);
	}
	
	public void dump(String filename) throws FileNotFoundException {
		PrintStream out = new PrintStream(new FileOutputStream(filename));
		dumpHeader(out);
		dumpCode(out);
		dumpFooter(out);
		dumpFrames();
	}
	
	private void dumpFrames() {
		for( IFrame frame: frames)
			frame.dump();
	}
	
	public void emit_bool(boolean val) {
		if(val)
			code.add("       iconst_1");
		else 
			code.add("       iconst_0");
	}

	public void emit_ifeq(String label) {
		code.add("       if_icmpeq "+label);
	}

	public void emit_ifne(String label) {
		code.add("       if_icmpne "+label);
	}
	
	public void emit_ifge(String label) {
		code.add("       if_icmpge "+label);
	}
	
	public void emit_ifgt(String label) {
		code.add("       if_icmpgt "+label);
	}
	
	public void emit_ifle(String label) {
		code.add("       if_icmple "+label);
	}
	
	public void emit_iflt(String label) {
		code.add("       if_icmplt "+label);
	}

	public StackFrame newFrame(ArrayList<Declaration> decl) {
		StackFrame frame = new StackFrame(decl, currentFrame);
		frames.add(frame);
		
		return frame;
	}
	
	public TypeFrame createRefFrame(IType refType) throws TypingException {
		refClass.put(refType.toString(), getClass(refType));
		//System.out.println("Type Check!!!! "+type.toString());
		TypeFrame typeFrame = new TypeFrame(getClass(refType));
		
		frames.add(typeFrame);
		return typeFrame;
	}
	
	public StackFrame getCurrentFrame() {
		return currentFrame;
	}

	public void aload_0() {
		code.add("       aload 0");
	}
	
	public void astore_0() {
		code.add("       astore 0");
	}

/*
	public void getfield(String string) {
		code.add("       getfield "+string);
	}

	public void add(String string) {
		code.add(string);
	}
*/
	public void emit_pushFrame(StackFrame nframe) {
		astore_0();
		currentFrame = nframe;
	}
	

	public void emit_popFrame() {
		if(currentFrame.parentFrame.number != 0){
			aload_0();
		    code.add("       checkcast " + currentFrame.getName());
		    code.add("       getfield " + currentFrame.getName()+"/SL L" + currentFrame.parentFrame.getName() + ";");
		} else {
			code.add("       aconst_null ; cleans the top of the stack");
		}
		astore_0();
		currentFrame = currentFrame.parentFrame;
		
	}

	public void emit_storeStack(StackFrame nframe, int offSet, String type) {
		code.add("       putfield frame_"+nframe.number+"/loc_"+offSet+" "+type);
	}

	public void checkcast(String frameName) {
		code.add("       checkcast "+frameName);
	}

	public void comment(String string) {
		code.add("       ; "+string);
	}

	public void emit_label(String label) {
		code.add("       "+label+":");
	}

	public void emit_pop() {
		code.add("       pop");
	}

	public void emit_goto(String label) {
		code.add("       goto "+label);
	}

	public void emit_newFrame(String name) {
		code.add("       new "+name+" ; create a frame");
	}

	public void emit_ivokeSFrame(String name) {
		code.add("       invokespecial "+name+"/<init>()V");
	}

	public void emit_putfield(String className, String type, String fieldName) {
		code.add("       putfield "+className+"/"+type+" "+fieldName);
	}
	
	public void emit_getfield(String string) {
		code.add("       getfield "+string);
	}

	public static String getClass(IType type) throws TypingException {
		if(type == IntType.singleton || type == BoolType.singleton)
			return "int";
		if(type instanceof RefType )
			return "ref_"+getClass(((RefType)type).getType());
		return null;
		/*
		String refType = "ref_";
		if(type == IntType.singleton || type == BoolType.singleton)
			refType += "int";
		else
			refType += getType(expr.getType(),((ASTVar) expr).getExpr());
		return refType;
		*/
	}
	
    public static String classToJasmin(ASTNode expr) throws TypingException{
    	if(expr.getType() instanceof RefType )
    		return "L"+getClass(expr.getType())+";";
    	else
    		return "I";
    }
    /*
    public static String typeToJasmin(ASTNode expr) throws TypingException{
    	if(expr.getType() instanceof RefType )
    		return "L"+getType(expr.getType(),expr)+";";
    	else
    		return "I";
    }
	*/
}