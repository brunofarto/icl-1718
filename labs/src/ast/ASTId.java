package ast;

import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import types.IType;
import types.TypingException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import util.CompilerEnv;
import util.CompilerEnv.Address;
import values.DynamicTypingException;
import values.IValue;

public class ASTId implements ASTNode {
	String id;
	IType idType;
	
	public ASTId(String id)
	{
		this.id = id;
	}

	public IValue eval(IEnvironment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException {
		return env.find(id);
	}

	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException {
		idType =  env.find(id);
		return idType;
	}

	public void compile(CodeBlock code,  CompilerEnv<IValue> env) throws UndeclaredIdentifierException, TypingException {
		Address addr = env.lookup(id);
		StackFrame frame = code.getCurrentFrame();
		int fnumber = frame.number;
		int jumps = addr.jumps;
		
		code.aload_0();
		while(jumps>0){
			code.emit_getfield("frame_"+frame.number+"/SL Lframe_"+frame.parentFrame.number+";");
			fnumber = frame.parentFrame.number;
			jumps--;
		}
		String type = CodeBlock.getClass(idType);
		code.emit_getfield("frame_"+fnumber+"/loc_"+addr.val+" "+(type.equals("int")?"I":"L"+type+";"));
	}
	
	public IType getType() throws TypingException {
		if(idType!=null)
			return idType;
		else 
			throw new TypingException("Uncheck type!");
	}
}
