package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.TypingException;
import util.CompilerEnv;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.IValue;

public class ASTBool implements ASTNode {
	boolean val;

	public ASTBool(boolean n) {
		val = n;
	}

	public String toString() {
		return Boolean.toString(val);
	}

	public IValue eval(IEnvironment<IValue> env) {
		return new BoolValue(val);
	}

	public IType typecheck(IEnvironment<IType> env) throws TypingException {
		return BoolType.singleton;
	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env) throws UndeclaredIdentifierException {
		code.emit_bool(val);
	}

	public IType getType(){
		return BoolType.singleton;
	}

}
