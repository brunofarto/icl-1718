package ast;

import compiler.CodeBlock;
import types.IType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.DynamicTypingException;
import values.IValue;

public interface ASTNode {
	
	IValue eval(IEnvironment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, DynamicTypingException;

	IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException;
	
	void compile(CodeBlock code, CompilerEnv<IValue> env) throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException;
	
	IType getType() throws TypingException;
}

