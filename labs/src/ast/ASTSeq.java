package ast;

import compiler.CodeBlock;
import types.IType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.DynamicTypingException;
import values.IValue;

public class ASTSeq implements ASTNode {
	ASTNode left, right;
	IType type;
	
	public ASTSeq(ASTNode left, ASTNode right){
		this.left = left;
		this.right = right;
	}

	public IValue eval(IEnvironment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, DynamicTypingException {
		left.eval(env);
		return right.eval(env);
	}

	public IType typecheck(IEnvironment<IType> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		left.typecheck(env);
		type = right.typecheck(env);
		return type;

	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env)
			throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
		left.compile(code,env);
		code.emit_pop();
		right.compile(code,env);
	}
	
    public String toString() {
    	return left.toString() + " ; " + right.toString();
    }

	public IType getType() throws TypingException {
		if(type!=null)
			return type;
		else 
			throw new TypingException("Uncheck type!");
	}
}
