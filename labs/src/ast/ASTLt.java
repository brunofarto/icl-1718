package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.IntType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.DynamicTypingException;
import values.IValue;
import values.IntValue;

public class ASTLt implements ASTNode {
	ASTNode left, right;
	IType type;

	public ASTLt(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	public String toString() {
		return left.toString() + " < " + right.toString();
	}

	public IValue eval(IEnvironment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IValue l = left.eval(env);
		IValue r = right.eval(env);
		if( l instanceof IntValue && r instanceof IntValue)
			return new BoolValue(((IntValue)l).getValue()<((IntValue)r).getValue());
		else 
			throw new DynamicTypingException("Wrong types in LesserThen");
	}

	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if( l == IntType.singleton && 
			r == IntType.singleton ){
			type = BoolType.singleton;
			return type;
		}
		else 
			throw new TypingException("Wrong types in LesserThen");
	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env) throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
		String label1 = code.labelFactory.getLabel();
		String label2 = code.labelFactory.getLabel();
		
		left.compile(code, env);
		right.compile(code, env);
		code.emit_ifge(label1);
		code.emit_push(1);
		code.emit_goto(label2);
		code.emit_label(label1); // label 1
		code.emit_push(0);
		code.emit_label(label2); // label 2
	}

	public IType getType() throws TypingException {
		if(type!=null)
			return type;
		else 
			throw new TypingException("Uncheck type!");
	}
}
