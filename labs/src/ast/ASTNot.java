package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.DynamicTypingException;
import values.IValue;

public class ASTNot implements ASTNode {
	ASTNode left;
	IType type;

	public ASTNot(ASTNode l) {
		left = l;
	}

	public String toString() {
		return "!" + left.toString();
	}

	public IValue eval(IEnvironment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IValue l = left.eval(env);
		if( l instanceof BoolValue)
			return new BoolValue(!((BoolValue)l).getValue());
		else 
			throw new DynamicTypingException("Wrong types in Not");
	}

	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType l = left.typecheck(env);
		
		if( l == BoolType.singleton){
			type = BoolType.singleton;
			return type;
		}
		else 
			throw new TypingException("Wrong types in Not");
	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env) throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
		left.compile(code, env);
		code.emit_not();
	}

	public IType getType() throws TypingException {
		if(type!=null)
			return type;
		else 
			throw new TypingException("Uncheck type!");
	}
}
