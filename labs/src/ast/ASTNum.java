package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
import util.CompilerEnv;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.IValue;
import values.IntValue;

public class ASTNum implements ASTNode {
	int val;

	public ASTNum(int n) {
		val = n;
	}

	public String toString() {
		return Integer.toString(val);
	}

	public IValue eval(IEnvironment<IValue> env) {
		return new IntValue(val);
	}

	public IType typecheck(IEnvironment<IType> env) throws TypingException {
		return IntType.singleton;
	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env) throws UndeclaredIdentifierException {
		code.emit_push(val);
	}

	public IType getType() {
		return IntType.singleton;
	}
}
