package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.DynamicTypingException;
import values.IValue;

public class ASTWhile implements ASTNode {
    ASTNode c,e;
    IType type;

    public ASTWhile(ASTNode condition,ASTNode exp){
        this.c=condition;
        this.e=exp;
    }

	public IValue eval(IEnvironment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, DynamicTypingException {
		BoolValue cond = (BoolValue)c.eval(env);
		while(cond.getValue()){
			e.eval(env);
			cond = (BoolValue)c.eval(env);
		}
		return cond; 
	}

	public IType typecheck(IEnvironment<IType> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		e.typecheck(env);
		type = c.typecheck(env);
		if(type==BoolType.singleton){
			return BoolType.singleton;
		} else
			throw new TypingException("Wrong types in While");

	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env)
			throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
		String start = code.labelFactory.getLabel();
		String   end = code.labelFactory.getLabel();
		
		code.emit_label(start);
    	c.compile(code, env);  	
    	code.emit_push(0);
    	code.emit_ifeq(end);// ifcmpqe
    	e.compile(code, env);
    	code.emit_pop();
    	code.emit_goto(start);
    	code.emit_label(end);
    	code.emit_push(0);
	}
	
    public String toString() {
        return "while "+c.toString()+" do "+e.toString()+" end ";
    }

	public IType getType() throws TypingException {
		if(type!=null)
			return type;
		else 
			throw new TypingException("Uncheck type!");
	}
}
