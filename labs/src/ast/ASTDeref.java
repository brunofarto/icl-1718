package ast;

import compiler.CodeBlock;
import types.IType;
import types.RefType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.DynamicTypingException;
import values.IValue;
import values.RefValue;

public class ASTDeref implements ASTNode {
    
	ASTNode expr;
    IType type;
    
    public ASTDeref(ASTNode expr){
        this.expr = expr;       
    }
    
	public IValue eval(IEnvironment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, DynamicTypingException {
		RefValue ref =(RefValue) expr.eval(env);
		return ref.getValue();
	}

	public IType typecheck(IEnvironment<IType> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType ref = expr.typecheck(env);
		
		if(ref instanceof RefType){
			this.type = ((RefType) ref).getType();
			return type;
		} else 
			throw new TypingException("Wrong types in Derref");	
	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env)
			throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
	  	String tClass = code.getClass(type);
		
		code.comment(" Start deref");
    	expr.compile(code, env);  	
    	code.emit_getfield("ref_"+tClass+"/value "+(tClass.equals("int")?"I":"L"+tClass+";"));
	}
	
    public String toString() {
    	return "*"+expr.toString();
    }
    
	public IType getType() throws TypingException {
		if(type!=null)
			return type;
		else 
			throw new TypingException("Uncheck type!");
	}
}
