package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.DynamicTypingException;
import values.IValue;

public class ASTIfThenElse implements ASTNode {
    private ASTNode c,e1,e2;
    IType type;

    public ASTIfThenElse(ASTNode condition,ASTNode exp1,ASTNode exp2){
        this.c=condition;
        this.e1=exp1;
        this.e2=exp2;
    }

	public IValue eval(IEnvironment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, DynamicTypingException {
		BoolValue cond = (BoolValue)c.eval(env);
		if(cond.getValue())
			return e1.eval(env);
		else
			return e2.eval(env); 
	}

	public IType typecheck(IEnvironment<IType> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType type = e1.typecheck(env);
		IType typeC = c.typecheck(env);

		if(type.equals(e2.typecheck(env))&&typeC==BoolType.singleton){
			return type;
		} else
			throw new TypingException("Wrong types in IF");
	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env)
			throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
		String label1 = code.labelFactory.getLabel();
		String label2 = code.labelFactory.getLabel();
		
		c.compile(code, env);

		code.emit_push(0);
		code.emit_ifeq(label1);// ifcmpqe
		e1.compile(code, env);
		code.emit_goto(label2);
		code.emit_label(label1);
		e2.compile(code, env);
		code.emit_label(label2);
	}
	
    public String toString() {
        return "if "+c.toString()+" then "+e1.toString()+" else "+e2.toString()+" end";
    }

	public IType getType() throws TypingException {
		if(type!=null)
			return type;
		else 
			throw new TypingException("Uncheck type!");
	}
}
