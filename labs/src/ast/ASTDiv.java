package ast;

import compiler.CodeBlock;
import types.IType;
import types.IntType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.DynamicTypingException;
import values.IValue;
import values.IntValue;

public class ASTDiv implements ASTNode {
	ASTNode left, right;
	IType type;

	public ASTDiv(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	public String toString() {
		return left.toString() + " / " + right.toString();
	}

	public IValue eval(IEnvironment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IValue l = left.eval(env);
		IValue r = right.eval(env);
		if( l instanceof IntValue && r instanceof IntValue)
			return new IntValue(((IntValue)l).getValue()/((IntValue)r).getValue());
		else 
			throw new DynamicTypingException("Wrong types in Division");
	}

	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if( l == IntType.singleton && 
			r == IntType.singleton ){
			type = IntType.singleton;
			return type;
		}
		else 
			throw new TypingException("Wrong types in Division");
	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env) throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
		left.compile(code, env);
		right.compile(code, env);
		code.emit_div();
	}

	public IType getType() throws TypingException {
		if(type!=null)
			return type;
		else 
			throw new TypingException("Uncheck type!");
	}
}
