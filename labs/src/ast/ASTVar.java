package ast;

import compiler.CodeBlock;
import compiler.CodeBlock.TypeFrame;
import types.BoolType;
import types.IType;
import types.IntType;
import types.RefType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.DynamicTypingException;
import values.IValue;
import values.RefValue;

public class ASTVar implements ASTNode {
    private ASTNode expr;
    private IType type;

    public ASTVar(ASTNode expr){
        this.expr = expr;
    }

	public IValue eval(IEnvironment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, DynamicTypingException {
		return new RefValue(expr.eval(env));
	}

	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException{
		//type = expr.typecheck(env);
		//return new RefType(type);
		type = new RefType(expr.typecheck(env));
		return type;
	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env)
			throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
		String tClass = code.getClass(type);
		
		code.createRefFrame(type);	
		
		code.emit_newFrame(tClass);
		code.emit_dup();
		code.emit_ivokeSFrame(tClass);
		code.emit_dup();
		
		expr.compile(code, env);
		
		code.emit_putfield(tClass,"value",(tClass.equals("ref_int"))?"I":"L"+tClass.substring(4)+";");
	}

    public String toString() {
        return "var("+expr.toString()+")";
    }
    
	public IType getType() throws TypingException {
		if(type!=null)
			return type;
		else 
			throw new TypingException("Uncheck type!");
	}
	
	public ASTNode getExpr(){
		return expr;
	}
}
