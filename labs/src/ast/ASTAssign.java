package ast;

import compiler.CodeBlock;
import types.IType;
import types.RefType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.DynamicTypingException;
import values.IValue;
import values.RefValue;

public class ASTAssign implements ASTNode {
    ASTNode lValue,rValue;
    IType type;
    
	public ASTAssign(ASTNode left, ASTNode right){
        this.lValue=left;
        this.rValue=right;
	}

	public IValue eval(IEnvironment<IValue> env)
			throws UndeclaredIdentifierException, DuplicateIdentifierException, DynamicTypingException {         
		RefValue ref = (RefValue) lValue.eval(env);
		IValue val = rValue.eval(env);
		ref.setVal(val);
		return val; 
	}

	public IType typecheck(IEnvironment<IType> env)
			throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType type = rValue.typecheck(env);
		RefType reftype = (RefType) lValue.typecheck(env);
		if(reftype.getType().equals(type)){
			this.type = type;
			return type;
		} else
			throw new  TypingException("Wrong types in Assign");
	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env)
			throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
		
		RefType refType = new RefType(type);
		String classType = code.getClass(refType);
		String valueType = code.getClass(type);
		
		code.comment(" Start Assign: "+refType);
		
		lValue.compile(code, env);
		
		code.emit_dup();

		rValue.compile(code, env);
		
		code.emit_putfield(classType,"value",(valueType.equals("int")?"I":"L"+valueType+";"));
		
		code.emit_getfield(classType+"/value "+(valueType.equals("int")?"I":"L"+valueType+";"));
		
		
	}
	
    public String toString() {
        return lValue.toString()+":="+rValue.toString();
    }

	public IType getType() throws TypingException {
		if(type!=null)
			return type;
		else 
			throw new TypingException("Uncheck type!");
	}
}
