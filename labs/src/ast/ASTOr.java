package ast;

import compiler.CodeBlock;
import types.BoolType;
import types.IType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.BoolValue;
import values.DynamicTypingException;
import values.IValue;

public class ASTOr implements ASTNode {
	ASTNode left, right;
	IType type;

	public ASTOr(ASTNode l, ASTNode r) {
		left = l;
		right = r;
	}

	public String toString() {
		return left.toString() + " || " + right.toString();
	}

	public IValue eval(IEnvironment<IValue> env) throws DynamicTypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IValue l = left.eval(env);
		IValue r = right.eval(env);
		if( l instanceof BoolValue && r instanceof BoolValue)
			return new BoolValue(((BoolValue)l).getValue()||((BoolValue)r).getValue());
		else 
			throw new DynamicTypingException("Wrong types in Or");
	}

	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IType l = left.typecheck(env);
		IType r = right.typecheck(env);
		
		if( l == BoolType.singleton && 
			r == BoolType.singleton ){
			type = BoolType.singleton;
			return type;
		}
		else 
			throw new TypingException("Wrong types in Or");
	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env) throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {
		left.compile(code, env);
		right.compile(code, env);
		code.emit_or();
	}

	public IType getType() throws TypingException {
		if(type!=null)
			return type;
		else 
			throw new TypingException("Uncheck type!");
	}
}
