package ast;

import java.util.ArrayList;

import compiler.CodeBlock;
import compiler.CodeBlock.StackFrame;
import types.IType;
import types.TypingException;
import util.CompilerEnv;
import util.DuplicateIdentifierException;
import util.IEnvironment;
import util.UndeclaredIdentifierException;
import values.DynamicTypingException;
import values.IValue;

public class ASTDecl implements ASTNode {
	public static class Declaration {
		public Declaration(String id, ASTNode def) {
			this.id = id;
			this.def = def;
		}
		String id;
		ASTNode def;
		
		
		public IType getType() throws TypingException {
			// TODO Auto-generated method stub
			return def.getType();
		}
		
		public ASTNode getExpr() throws TypingException {
			// TODO Auto-generated method stub
			return def;
		}
		
	}
	
	ASTNode body;
	ArrayList<Declaration> decls;
	IType type;
	
	public ASTDecl() {
		decls = new ArrayList<>();
	}
	
	public void addBody(ASTNode body) {
		this.body = body;
	}
	
	public void newBinding(String id, ASTNode e) {
		decls.add(new Declaration(id,e));
	}

	public IValue eval(IEnvironment<IValue> env) throws UndeclaredIdentifierException, DuplicateIdentifierException, DynamicTypingException {
		IEnvironment<IValue> newenv = env.beginScope();
		
		for( Declaration d: decls)
			newenv.assoc(d.id, d.def.eval(env));
		
		IValue value = body.eval(newenv);
		
		env.endScope();
		
		return value;
	}

	public IType typecheck(IEnvironment<IType> env) throws TypingException, UndeclaredIdentifierException, DuplicateIdentifierException {
		IEnvironment<IType> newenv = env.beginScope();
		
		for( Declaration d: decls)
			newenv.assoc(d.id, d.def.typecheck(env));
		
		this.type = body.typecheck(newenv);
		
		env.endScope();
		
		return type;	
	}

	public void compile(CodeBlock code, CompilerEnv<IValue> env) throws UndeclaredIdentifierException, TypingException, DuplicateIdentifierException {		
		StackFrame nframe = code.newFrame(this.decls);
		CompilerEnv<IValue> newEnv = env.beginScope();
		int offSet = 0;
		
		code.emit_newFrame("frame_"+nframe.number);
		code.emit_dup();
		code.emit_ivokeSFrame("frame_"+nframe.number);
		
		if(nframe.parentFrame.number!=0){
			code.emit_dup();
			code.aload_0();
			code.emit_putfield("frame_"+nframe.number,"SL", "Lframe_"+nframe.parentFrame.number+";");
		}
		
		for(Declaration decl:decls){
			offSet = newEnv.addSlot(decl.id);
			String tClass = code.getClass(decl.getType());
			
			code.emit_dup();

			decl.def.compile(code,env);
			code.emit_storeStack(nframe,offSet,(tClass.equals("int")?"I":"L"+tClass+";"));
			offSet++;
		}
		
		// begin scope (push SP)
		code.emit_pushFrame(nframe);
		// associate id to address
		body.compile(code,newEnv);
		// end scope (pop SP)
		code.emit_popFrame();
		newEnv.endScope();
		
	}

	public IType getType() throws TypingException {
		if(type!=null)
			return type;
		else 
			throw new TypingException("Uncheck type!");
	}
}
