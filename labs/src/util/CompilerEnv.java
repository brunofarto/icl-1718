package util;

import java.util.ArrayList;

import util.CompilerEnv.Address;

@SuppressWarnings("rawtypes")
public class CompilerEnv<T> extends Environment {
	
	public class Address{
		public int jumps;
		public T val;
		
		public Address(int jumps, T val){
			this.jumps = jumps;
			this.val = val;
		}
	}
	
	public CompilerEnv() {
		super();
	}
	
	public CompilerEnv(CompilerEnv<T> up) {
		this();
		this.up = up;
	}
	
	public Address lookup(String id) throws UndeclaredIdentifierException {
		int jumps = 0;
		
		Environment<T> current = this;
		while(current != null) {
			for(Assoc<T> assoc: current.assocs)
				if( assoc.id.equals(id))
					return new Address(jumps,assoc.value);
			current = current.up;
			jumps++;
		}
		throw new UndeclaredIdentifierException(id);
	}
	
	public CompilerEnv<T> beginScope() {
		return new CompilerEnv<T>(this);
	}
	/*
	public CompilerEnv<T> endScope() {
		return (CompilerEnv<T>) up;
	}
	*/
	
	public int addSlot(String id) throws DuplicateIdentifierException {
		Environment<T> current = this;
		for(Assoc<T> assoc: current.assocs)
			if(assoc.id.equals(id))
				throw new DuplicateIdentifierException(id);
		int value = assocs.size();
		
		assocs.add(new Assoc(id,value));

		return value;
	}
	
}
