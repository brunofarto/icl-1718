package types;

public class BoolType implements IType {
	public static final BoolType singleton = new BoolType();
	private BoolType(){}
	
	public String toString(){return "bool";}
}
