package types;

public class RefType implements IType {
	//public static IType singleton = new RefType();
	private final IType type;
	
	public RefType(IType type){
		this.type = type;
	}
	
	public String toString(){
		return "ref("+type.toString()+")";
	}
	
	public IType getType(){
		return type;
	}
}
